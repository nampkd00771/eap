package com.example.eaptest.repository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EaptestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EaptestApplication.class, args);
    }

}
